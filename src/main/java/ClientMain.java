import client.Client;
import server.Server;

public class ClientMain {
    public static void main(String[] args) {
        Server client = new Server(5556);
        client.execute();
    }
}
