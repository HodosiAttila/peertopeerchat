package client;

import communication.Actions;
import communication.Message;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * This thread is responsible for reading user's input and send it
 * to the server.
 * It runs in an infinite loop until the user types 'bye' to quit.
 */
public class WriteThread extends Thread {
    private PrintWriter writer;
    private Socket socket;
    private Client client;

    /**
     * @param socket - writing to server
     * @param client
     */
    public WriteThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;

        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException ex) {
            System.out.println("Error getting output stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {

//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Scanner scanner = new Scanner(System.in);

//        String userName = console.readLine("\nEnter your name: ");
//        client.setUserName(userName);
//        writer.println(userName);

        String text;

        do {
//            text = console.readLine("[" + userName + "]: ");
            text = scanner.nextLine();
            System.out.println("you just wrote " + text);
            Message message = new Message(Actions.MESSAGE.getActionCode(), text, "");
            writer.println(message.convertToJson());

        } while (!text.equals("bye"));

        try {
            socket.close();
        } catch (IOException ex) {

            System.out.println("Error writing to server: " + ex.getMessage());
        }
    }
}
