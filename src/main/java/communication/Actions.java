package communication;

public enum Actions {
    CONNECT(1),
    ACK(2),
    MESSAGE(3),
    BYE(4);

    private final int actionCode;

    Actions(int actionCode) {
        this.actionCode = actionCode;
    }

    public int getActionCode() {
        return actionCode;
    }
}
