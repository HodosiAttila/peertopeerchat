package communication;

import java.util.Arrays;
import java.util.Optional;

public enum Users {
    Alice("127.0.0.1", 5555),
    Ramona("192.168.43.20", 5556);

    private String userIP;
    private int port;

    Users(String userIP, int port) {
        this.userIP = userIP;
        this.port = port;
    }

    public String getUserIP() {
        return userIP;
    }

    public int getPort() {
        return port;
    }

    public static String getUserName(String ip) {
         Optional<Users> optional = Arrays.stream(Users.values())
                .filter(user -> user.userIP.equals(ip))
                .findFirst();

         return optional.isPresent() ? optional.get().toString() : "";
    }
}
