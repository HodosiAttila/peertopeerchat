package communication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Message {
    private int action;
    private String message;
    private String parameters;

    private Message() {
    }

    public Message(int action, String message, String parameters) {
        this.action = action;
        this.message = message;
        this.parameters = parameters;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String convertToJson() {
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static Message convertToMessage(String jsonText) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Message message = objectMapper.readValue(jsonText, Message.class);
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "action=" + action +
                ", message='" + message + '\'' +
                ", parameters='" + parameters + '\'' +
                '}';
    }
}
