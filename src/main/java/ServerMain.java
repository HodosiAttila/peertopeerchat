import server.Server;

public class ServerMain {
    //sudo lsof -i:5555
    final static int PORT = 5555;

    public static void main(String[] args) {
        Server server = new Server(PORT);
        server.execute();
    }
}
