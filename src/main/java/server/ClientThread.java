package server;

import communication.Actions;
import communication.Message;
import communication.Users;
import connection.ConnectionManagerReader;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;

public class ClientThread extends Thread {
    private Socket clientSocket;
    private ConnectionManagerReader connectionManager;
    private PrintWriter writer;
    private HashMap<String, Socket> serverSockets;

    public ClientThread(Socket socket, ConnectionManagerReader connectionManager, HashMap<String, Socket> serverSockets) {
        this.clientSocket = socket;
        this.connectionManager = connectionManager;
        this.serverSockets = serverSockets;
    }

    public void run() {
        try {
            InputStream input = clientSocket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            OutputStream output = clientSocket.getOutputStream();
            writer = new PrintWriter(output, true);

            String clientMessageJson;
            clientMessageJson = reader.readLine();

            Message message;
            message = Message.convertToMessage(clientMessageJson);

            // initializing connection
            if (message.getAction() == Actions.CONNECT.getActionCode()) {
//                Message ackMessage = new Message(Actions.ACK.getActionCode(), "ackk", "");
//                writer.println(ackMessage.convertToJson());
            } else {
                System.out.println("Connection isn't init with action 1, closing connection...");
                return;
            }

            InetSocketAddress sockaddr = (InetSocketAddress) clientSocket.getRemoteSocketAddress();
            InetSocketAddress sockaddr2 = (InetSocketAddress) clientSocket.getLocalSocketAddress();
            System.out.println(sockaddr);
            System.out.println(sockaddr2);
            String clientIp = sockaddr.getAddress().toString().substring(1);
            String clientName = Users.getUserName(clientIp);

            System.out.println(clientName + " has connected!");
            serverSockets.put(clientName, clientSocket);

            // communication
            do {
                clientMessageJson = reader.readLine();
                message = Message.convertToMessage(clientMessageJson);
                System.out.println("[" + clientName + "]: " + message.getMessage());
//                server.broadcast(serverMessage, this);

            } while (!message.getMessage().equals("bye"));
//
////          server.removeUser(userName, this);
            clientSocket.close();
            System.out.println(clientName + " has quitted.");
////          server.broadcast(serverMessage, this);
//
        } catch (IOException ex) {
            System.out.println("Error in UserThread: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
//
//    /**
//     * Sends a list of online users to the newly connected user.
//     */
//    void printUsers() {
//        if (server.hasUsers()) {
//            writer.println("Connected users: " + server.getUserNames());
//        } else {
//            writer.println("No other users connected");
//        }
//    }
//
//    /**
//     * Sends a message to the client.
//     */
//    void sendMessage(String message) {
//        writer.println(message);
//    }
}
