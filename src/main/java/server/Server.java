package server;

import connection.ConnectionManagerReader;
import connection.ConnectionManagerWriter;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    private int port;
//    private Set<String> clientNames = new HashSet<>();
    private Set<ClientThread> clientThreads = new HashSet<>();
    private HashMap<String, Socket> serverSockets = new HashMap<>();

    public Server(int port) {
        this.port = port;
    }

    public void execute() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Chat Server is listening on port " + port);

            // read messages from others
            ConnectionManagerReader reader = new ConnectionManagerReader(serverSocket, this, serverSockets);
            reader.start();

            // write messages to others
            ConnectionManagerWriter writer = new ConnectionManagerWriter(serverSocket, this, serverSockets);
            writer.start();
        } catch (IOException ex) {
            System.out.println("Error in the server: " + ex.getMessage());
            ex.printStackTrace();
        }
//        try {
//            Socket socket = new Socket(serverIP, serverPort);
//
//            System.out.println("Connected to the chat server");
//
//            new ReadThread(socket, this).start();
//            new WriteThread(socket, this).start();
//
//        } catch (UnknownHostException ex) {
//            System.out.println("Server not found: " + ex.getMessage());
//        } catch (IOException ex) {
//            System.out.println("I/O Error: " + ex.getMessage());
//        }
    }

    public void addClient(ClientThread client) {
        clientThreads.add(client);
    }
//
//    /**
//     * Delivers a message from one user to others (broadcasting)
//     */
//    void broadcast(String message, UserThread excludeUser) {
//        for (UserThread aUser : userThreads) {
//            if (aUser != excludeUser) {
//                aUser.sendMessage(message);
//            }
//        }
//    }
//
//    /**
//     * Stores username of the newly connected client.
//     */
//    void addClientName(String userName) {
//        clientNames.add(userName);
//    }
//
//    /**
//     * When a client is disconneted, removes the associated username and UserThread
//     */
//    void removeUser(String userName, UserThread aUser) {
//        boolean removed = userNames.remove(userName);
//        if (removed) {
//            userThreads.remove(aUser);
//            System.out.println("The user " + userName + " quitted");
//        }
//    }
//
//    Set<String> getUserNames() {
//        return this.userNames;
//    }
//
//    /**
//     * Returns true if there are other users connected (not count the currently connected user)
//     */
//    boolean hasUsers() {
//        return !this.userNames.isEmpty();
//    }
}