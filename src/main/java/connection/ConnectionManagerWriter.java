package connection;

import client.ReadThread;
import client.WriteThread;
import communication.Actions;
import communication.Message;
import communication.Users;
import server.ClientThread;
import server.Server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class ConnectionManagerWriter extends Thread{
    private final ServerSocket serverSocket;
    private final Server server;

    private final HashMap<String, Socket> serverSockets;

    public ConnectionManagerWriter(ServerSocket serverSocket, Server server, HashMap<String, Socket> serverSockets) {
        this.serverSocket = serverSocket;
        this.server = server;
        this.serverSockets = serverSockets;
    }

    @Override
    public void run() {
        while (true) {
            // hello andrei -> sa vada daca andrei e ok, il cauta in enum si dupa face o cerere catre server.
            // andrei: mesaj
            // hello Alex
            // alex: mesaj
            // andrei: mesaj2
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String input = scanner.nextLine();
                if(input.equals("!byebye")){
                    break;
                }

                String[] tokens = input.split(":");
                if(tokens.length == 2) {
                    String commandName = tokens[0];
                    String message = tokens[1];

                    if(commandName.equals("!hello")) {
                        // create connection
                        Socket socket = null;
                        try {
                            socket = new Socket(Users.valueOf(message).getUserIP(), Users.valueOf(message).getPort());
                            Message messageConnect = new Message(Actions.CONNECT.getActionCode(), "", "");

                            OutputStream output = socket.getOutputStream();
                            PrintWriter writer = new PrintWriter(output, true);
                            writer.println(messageConnect.convertToJson());

                            // here to check for ack

                            System.out.println("You can now talk to " + message);
                            // add
                            serverSockets.put(message, socket);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

//                        new ReadThread(socket, this).start();
//                        new WriteThread(socket, this).start();
                    } else {
                        // send message
                        System.out.println(serverSockets);
                        Socket socket = serverSockets.get(commandName);
                        Message messageToServer = new Message(Actions.MESSAGE.getActionCode(), message, "");

                        OutputStream output = null;
                        try {
                            output = socket.getOutputStream();
                            PrintWriter writer = new PrintWriter(output, true);
                            writer.println(messageToServer.convertToJson());
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }

            }


        }
    }
}
