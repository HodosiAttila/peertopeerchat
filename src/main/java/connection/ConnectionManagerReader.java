package connection;

import server.ClientThread;
import server.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ConnectionManagerReader extends Thread {
    private final ServerSocket serverSocket;
    private final Server server;

    private final HashMap<String, Socket> serverSockets;

    public ConnectionManagerReader(ServerSocket serverSocket, Server server, HashMap<String, Socket> serverSockets) {
        this.serverSocket = serverSocket;
        this.server = server;
        this.serverSockets = serverSockets;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client is trying to connect...");
                ClientThread newClient = new ClientThread(clientSocket, this, serverSockets);
                server.addClient(newClient);
//                serverSockets.put()
                newClient.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
